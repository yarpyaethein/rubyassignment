require "sinatra"
require "sinatra/namespace"
require "sinatra/activerecord"
require "./models.rb"
set :database, "sqlite3:mybooksdb.sqlite3"


  get '/' do
  	@books = Book.all
    # @books.to_json
      erb :index
  end

  post '/post' do
  	@books = Book.create(name: params[:name])
  	erb :show
  end

  put '/update/:id' do
  @books = Book.find(params[:id])
  @books.update(name: params[:name])
  redirect '/'
  end

  get '/books/new' do
    erb :new
  end

  get '/books/:id/delete' do
  	Book.delete(params[:id])
  	redirect '/'
  end

  get '/books/:id' do
    @books =Book.find(params[:id])
    erb :show
  end

  get '/books/:id/edit' do
    @books =Book.find(params[:id])
    erb :edit
  end
